import folium
from folium import plugins
import requests
import json

resp = input('Mapas: Mundi/Brasil\n')

if 'BRASIL' in resp.upper():
    url = "https://xx9p7hp1p7.execute-api.us-east-1.amazonaws.com/prod/PortalMapa"

    headers = {
      'authority': 'xx9p7hp1p7.execute-api.us-east-1.amazonaws.com',
      'accept': 'application/json, text/plain, */*',
      'sec-fetch-dest': 'empty',
      'x-parse-application-id': 'unAFkcaNDeXajurGB7LChj8SgQYS2ptm',
      'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
      'origin': 'https://covid.saude.gov.br',
      'sec-fetch-site': 'cross-site',
      'sec-fetch-mode': 'cors',
      'referer': 'https://covid.saude.gov.br/',
      'accept-language': 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7'
    }

    resultList = requests.request("GET", url, headers=headers).json()['results']

    m = int(input('Mapas disponíveis:\n1 - Calor\n2 - Quantitativo\n'))

    mapa = folium.Map(location=[-15.788497, -47.879873], zoom_start=0o4)
    if m == 1:
        coordenadas = []
        cont = 0
        for region in resultList:
            print(f'Colocando {int(region["qtd_confirmado"])} pontos no mapa, região: {region["nome"]}')
            for x in range(int(region['qtd_confirmado'])):
                cont += 0.000001
                coordenadas.append([float(region['latitude']) + cont, float(region['longitude']) + cont])

        mapa.add_child(plugins.HeatMap(coordenadas))
    elif m == 2:
        for region in resultList:
            folium.Circle(
                location=[region['latitude'], region['longitude']],
                popup=f"{region['nome']}\nConfirmados:{region['qtd_confirmado']}\nLetalidade:{region['letalidade']}\nÓbitos:{region['qtd_obito']}",
                radius=region['qtd_confirmado'] * 50,
                color='crimson',
                fill=True,
                fill_color='crimson'
            ).add_to(mapa)

else:
    url = "https://elpais.com/infografias/2020/02/coronavirus-crisis/expansion/brasil/coronavirus_to_map_brasil.json?v=1586433858656"

    response = requests.request("GET", url).json()[0]
    temp = json.loads(response)
    mapa = folium.Map(location=[20, 0], zoom_start=2)

    tipo_mapa = int(input('Qual tipo de mapa deseja visualizar:\n1- Mapa de Calor\n2- Mapa quantitativo'))

    if tipo_mapa == 2:
        for local in temp['confirmed']:
            print(f'Adicionando {local[list(local.keys())[-1]]} pontos na {local["Country/Region"]}')
            if 'Province/State' in local:
                folium.Circle(
                    location=[local['Lat'], local['Long']],
                    popup=f"{local['Country/Region']}, {local['Province/State']} Confirmados:{local[list(local.keys())[-1]]}",
                    radius=local[list(local.keys())[-1]],
                    color='crimson',
                    fill=True,
                    fill_color='crimson'
                ).add_to(mapa)
            else:
                folium.Circle(
                    location=[local['Lat'], local['Long']],
                    popup=f"{local['Country/Region']}\nConfirmados: {local[list(local.keys())[-1]]}",
                    radius=local[list(local.keys())[-1]],
                    color='crimson',
                    fill=True,
                    fill_color='crimson'
                ).add_to(mapa)
    else:
        coordenadas = []
        cont = 0

        for region in temp['confirmed']:
            print(f'Colocando {region[list(region.keys())[-1]]} pontos no mapa, região: {region["Country/Region"]}')

            for x in range(int(region[list(region.keys())[-1]])):
                cont += 0.000001
                coordenadas.append([float(region['Lat']) + cont, float(region['Long']) + cont])

        mapa.add_child(plugins.HeatMap(coordenadas))

mapa
mapa.save('index.html')